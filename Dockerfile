FROM ubuntu:20.10

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /app
COPY ./ /app

RUN \
  pwd && \
  ls -la && \
  chmod +x install_dependencies.sh && \
  ./install_dependencies.sh
