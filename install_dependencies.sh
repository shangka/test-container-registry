#!/bin/bash

apt-get update
apt-get install -y cmake python3 python3-sphinx can-utils libzmq3-dev git ruby doxygen graphviz mscgen libsocketcan-dev gcovr

pip3 install sphinx-rtd-theme recommonmark

mkdir /usr/local/lib64
echo "/usr/local/lib64">> /etc/ld.so.conf

# Installing the lib ftd
apt-get install -y wget
wget https://www.ftdichip.com/Drivers/D2XX/Linux/libftd2xx-x86_64-1.4.8.gz
tar -xzvf libftd2xx-x86_64-1.4.8.gz
cd release/build
cp libftd2xx.* /usr/local/lib64/
chmod 755 /usr/local/lib64/libftd2xx.so.1.4.8
ln -sf /usr/local/lib64/libftd2xx.so.1.4.8 /usr/local/lib64/libftd2xx.so
ldconfig
cd ../../

# Installing the lib MPSSE
git clone https://github.com/ImpruvIT/libMPSSE.git
sed -i 's/typedef unsigned char\tbool/ /' libMPSSE/Release/include/libMPSSE_spi.h
sed -i 's/bool/ unsigned char/' libMPSSE/Release/include/libMPSSE_spi.h
cd libMPSSE/LibMPSSE/Build/Linux/
make
cp libMPSSE.* /usr/local/lib64/
chmod 755 /usr/local/lib64/libMPSSE.so
ldconfig
cd ../../../../
cd libMPSSE/Release/include
cp libMPSSE_spi.h /usr/local/include
cp linux/ftd2xx.h /usr/local/include
cp linux/WinTypes.h /usr/local/include
cd ../../../
